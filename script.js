const getCube = Math.pow(2,3);
console.log(`The cube of 2 is ${getCube}`);

// Create a variable address with a value of an array containing details of an address.


const address = ["258", "Washington Ave NW", "California", 9001];

const [steertNum, street, state, zip] = address;

console.log(`I live at ${steertNum} ${street} ${state} ${zip}`);

/*
7. Create a variable animal with a value of an object data type with different animal details as it’s properties.
8. Destructure the object and print out a message with the details of the animal using Template Literals.
*/

const animal = {
	animalName: `Lolong`,
	animalType: `Saltwater Crocodile`,
	weight: `1075 kgs`,
	measurement: `20 ft 3 in`
};

const {animalName, animalType, weight, measurement} = animal;
console.log(`${animalName} is a ${animalType}. He weighed at ${weight} with a measurement of ${measurement}.`);

/*
9. Create an array of numbers.
10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.
11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array
*/

// const arrayNum = [1,2,3,4,5,15];

const arrayNum = [1,2,3,4,5,15];

arrayNum.forEach((num) => console.log(`${num}`))

/*
12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
13. Create/instantiate a new object from the class Dog and console log the object.
*/

class dog{
	constructor(brand, name, year){
		this.name = brand;
		this.age = name;
		this.breed = year;
	}
}

const myDog = new dog();

myDog.name = `Frankie`;
myDog.age = 5;
myDog.breed = `Miniature Dachshund`

console.log(myDog);

	


